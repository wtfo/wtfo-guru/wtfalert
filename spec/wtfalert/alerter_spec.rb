# frozen_string_literal: true

require 'wtfalert/alerter'
require 'tempfile'

# temp logfile since for some reason I have not figured out yet
# I get a syslog alredy open error when using syslog in rspec

tmplog = Tempfile.new(['rspec-script', '.log'])

dir = Dir.tmpdir

RSpec.describe Wtfalert::Alerter do
  let(:alerter) do
    described_class.new(
      :caller => File.basename(__FILE__),
      :level => 'debug',
      :screen => true,
      :options => { :store => dir },
      :logfile => tmplog.path
    )
  end

  before :context do # rubocop:disable RSpec/BeforeAfterAll
    alert_store = File.join(dir, 'alerts.yaml')
    File.unlink(alert_store) if File.exist?(alert_store)
  end

  it 'creates alerter' do
    expect(alerter).not_to be_nil
  end

  it 'raises and sends test alert' do
    alerter.raise_alert(:key => 'rspec.test')
    expect(alerter.status).to eq('raised: 1 cleared: 0 sent: 1 throttled: 0 errors: 0')
  end

  it 'raises and throttles test alert' do
    alerter.raise_alert(:key => 'rspec.test')
    expect(alerter.status).to eq('raised: 1 cleared: 0 sent: 0 throttled: 1 errors: 0')
  end

  it 'clears test alert' do
    alerter.clear(:key => 'rspec.test')
    expect(alerter.status).to eq('raised: 0 cleared: 1 sent: 0 throttled: 0 errors: 0')
  end
end
